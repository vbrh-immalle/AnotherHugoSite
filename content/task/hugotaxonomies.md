+++
date = "2015-10-23T09:35:48+02:00"
title = "hugotaxonomies"
categories = ["hugo"]
+++

# Taxonomies

Without configuration, Hugo uses 2 taxonomies:

- tags
- categories

# Usage

You make a document part of a taxonomy through front-matter:

```
+++
categories = ["hugo"]
tags = ["september"]
+++
```

# Defining own taxonomies

For a simpler understanding of taxonomies it is useful to be explicit about them
and remove the default `categories` and `tags`.

In `config.toml` you can put:

```
[taxonomies]
skill = "skills"
```

As soon as you do this, the default `tags` and `categories` are no longer
available. (You can still use them as front-matter and as such they will still
be available in layouts e.g. `{{ range .Params.categories }}`.
They are just not considered taxonomies anymore. This has some serious consequences.)

You can check the output of `hugo`:

```
0 draft content
0 future content
7 pages created
0 paginator pages created
6 skills created
```

which shows how many **terms** each of your taxonomies have. In this example
we used 6 different skills. They can all be found in the
`content/teammember/*.md`-files.

The different skills are defined in front-matter (If you add them up, you'll
have 6.)

`content/teammember/alice.md` :

```
+++
skills = ["encryption"]
+++
```

`content/teammember/bob.md` :

```
+++
skills = ["encryption"]
+++
```

`content/teammember/fred.md` :

```
+++
skills = ["javascript", "html", "css"]
+++
```


`content/teammember/john.md` :

```
+++
skills = ["C#", ".Net"]
+++
```

# Categories and tags

Now think about what happens if you put no `[Taxonomies]` in `config.toml` and the `categories` and `tags`-terms are into play.

The output could be:

```
0 draft content
0 future content
7 pages created
0 paginator pages created
2 categories created
3 tags created
```

But what exactly does this mean? What exactly has been created?


