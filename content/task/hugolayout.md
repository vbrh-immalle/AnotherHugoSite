+++
date = "2015-10-22T21:32:16+02:00"
title = "hugolayout"
categories = ["hugo"]
+++

# How to add layout files

The 2 most important layout files are:

- `layouts/index.html` : the template for the homepage
- `layouts/_default/single.html` : the template for one document

But you can also give seperate section's a different template:

- `layouts/<section>/single.html`

For convenience, there are a few more typical filenames:

- `li.html`
- `summary.html`
- ...

They are usually invoked from other templates, for example with
`{{ Render "li" }}` or `{{ Render "li" . }}` (the latter form passes
the current object as a parameter to the template which is very useful to access
the front-matter or built-in variables)

# What to put in layout files?

You can access all front-matter in every `single.html`.

There are also *Site-wide* variables. Those you can use everywhere and
thus also in `index.html`.

Remember: there is a complete template language at your disposal.
In practice the `{{ range ... }}`-operator is one of the most useful.

```
{{ range .Site.Pages }}
    <li>{{ . }}</li>
{{ end }}
```

This prints all the `.Site.Params`-objects.

Each object can have other properties:

```
{{ range .Site.Pages }}
    <li><a href={{ .Permalink }}">{{ .Title }}</a></li>
{{ end }}
`
```

As such, you can generate html-fragments!


