+++
date = "2015-10-22T21:32:06+02:00"
title = "hugocontent"
categories = ["hugo"]
+++

# How to add content

You have 2 options:

- use `hugo new <section>/<document>.md`
    - it will be created using the `archetypes/<section>.md`-file
- manually save the markdown-document as `content/<section>/<document>.md`
    - you have to add front-matter manually as well
